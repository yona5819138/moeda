<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            
            ['attribute' => 'urgency' , 'value' => 'user.urgency'],
            'created_at',
            'updated_at',
          
            ['attribute' => 'created_by' , 'value' => 'user1.name'],
    
            ['attribute' => 'updated_by' , 'value' => 'user2.name'],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
