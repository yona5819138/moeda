<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\models\User;
use app\models\Urgency;
use yii\db\ActiveRecord;
use yii\db\Expression;
/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $urgency
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    
                ],
                'value' => new Expression('NOW()'),
            ],
          'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],          
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name', 'urgency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urgency' => 'Urgency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(Urgency::className(), ['id' => 'urgency']);
    }
    public function getUser1()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUser2()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
