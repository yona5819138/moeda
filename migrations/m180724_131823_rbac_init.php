<?php

use yii\db\Migration;

/**
 * Class m180724_131823_rbac_init
 */
class m180724_131823_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    { $auth = Yii::$app->authManager;//חובה תמיד
        
      
        $manager = $auth->createRole('manager');
        $auth->add($manager);

        $employee  = $auth->createRole('employee ');
        $auth->add($employee );
   

        $auth->addChild($manager, $employee);

   ////////////////////////////////////////////////////////////////////////////
        
   $createTask = $auth->createPermission('createTask');
   $auth->add($createTask);

   $indexTask = $auth->createPermission('indexTask');
   $auth->add($indexTask);

   $viewtask = $auth->createPermission('viewtask');
   $auth->add($viewtask);

   $deleteTask = $auth->createPermission('deleteTask');
   $auth->add($deleteTask);

   $updateTask = $auth->createPermission('updateTask');
   $auth->add($updateTask);      
   
   $indexUser = $auth->createPermission('indexUser');
   $auth->add($indexUser);  
   
   $viewUser = $auth->createPermission('viewUser');
   $auth->add($viewUser);

   $updateUser = $auth->createPermission('updateUser');
   $auth->add($updateUser);

   $updateOwnUser = $auth->createPermission('updateOwnUser');

   $rule = new \app\rbac\EmploeeRule;// לקרוא לזה איך שקראנו לו בתוך תיקיית rbac
   $auth->add($rule);
   
   $updateOwnUser->ruleName = $rule->name;  //מה שאנחנו עושים עליו את התנאי יופיע ראשון               
   $auth->add($updateOwnUser);                                                    
   
   $auth->addChild($manager, $updateUser);
   $auth->addChild($manager, $deleteTask);
   $auth->addChild($manager, $updateTask);
   $auth->addChild($employee, $createTask);
   $auth->addChild($employee, $indexTask);
   $auth->addChild($employee, $viewtask); 
   $auth->addChild($employee, $indexUser);
   $auth->addChild($employee, $viewUser);
   $auth->addChild($employee, $updateOwnUser);
   $auth->addChild($updateOwnUser, $updateUser);   //קודם כל המצומצם ואז הגדול
   
   
   
   
   
   
   
   
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180724_131823_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_131823_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
