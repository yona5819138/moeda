<?php

use yii\db\Migration;

/**
 * Class m180724_114420_init
 */
class m180724_114420_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->insert('urgency', [
                'urgency' => 'critical',
            ]);
    
            $this->insert('urgency', [
                'urgency' => 'normal',
            ]);
    
            $this->insert('urgency', [
                'urgency' => 'low',
            ]);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180724_114420_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_114420_init cannot be reverted.\n";

        return false;
    }
    */
}
